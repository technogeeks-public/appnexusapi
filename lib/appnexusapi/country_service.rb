class AppnexusApi::CountryService < AppnexusApi::Service

  def initialize(connection)
    @read_only = true
    super(connection)
  end

  def plural_name
    "countries"
  end

end
