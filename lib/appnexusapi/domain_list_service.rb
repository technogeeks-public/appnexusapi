class AppnexusApi::DomainListService < AppnexusApi::Service
  def name
    "domain-list"
  end
  
  def plural_name
    "domain-lists"
  end
end
