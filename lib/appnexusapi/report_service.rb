class AppnexusApi::ReportService < AppnexusApi::Service
  def make_async_report_request(uri_params={}, attributes={})
    raise(AppnexusApi::NotImplemented, "Service is read-only.") if @read_only
    attributes = { name => attributes }
    uri_querystring = uri_params.map{|k,v| [CGI.escape(k.to_s), "=", CGI.escape(v.to_s)]}.map(&:join).join("&")
    uri_sum = uri_querystring.length > 0 ? uri_suffix + "?"+ uri_querystring : uri_suffix
    response = @connection.post(uri_sum, attributes)
    response["response"]["report_id"]
  end

end
